package com.biz.cooey;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

class BPDataCustomSerializer implements JsonSerializer<Object>{
	 
	@Override
	public JsonElement serialize(Object arg0, Type arg1,
			JsonSerializationContext arg2) {
		
		return new JsonPrimitive(arg0.toString());
	}               
}

