package com.biz.cooey;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.lifesense.ble.LsBleManager;
import com.lifesense.ble.commom.DeviceType;


public class CooeyBleDeviceFactory {

	private List<DeviceType> devList = new ArrayList<DeviceType>();
	private BleDeviceManager mDeviceManager;

	
	public CooeyBleDeviceFactory(int[] dtypes){
		addDevices(dtypes);
		//mDeviceManager = getBleDeviceManager();
	}
	
	public BleDeviceManager getBleDeviceManager(){
		//if Lifesense
		return new CooeyBleDeviceManager(devList);
		//if TH
		
		//if Philips
	}
	
	private void addDevices(int[] devices){
		
		/*devList.add(DeviceType.SPHYGMOMANOMETER);
		devList.add(DeviceType.FAT_SCALE);
		devList.add(DeviceType.WEIGHT_SCALE);
		devList.add(DeviceType.HEIGHT_RULER);
		devList.add(DeviceType.PEDOMETER);*/
		
		for(int d : devices){
			
			if(d == CooeyDeviceType.WEIGHT_SCALE){
				devList.add(DeviceType.WEIGHT_SCALE);
			}
			else if(d==CooeyDeviceType.BP_METER){
				devList.add(DeviceType.SPHYGMOMANOMETER);
			}
			else if(d==CooeyDeviceType.FAT_SCALE){
				devList.add(DeviceType.FAT_SCALE);
			}else if(d==CooeyDeviceType.WEIGHT_AND_FAT_SCALE){
				devList.add(DeviceType.FAT_SCALE);
				devList.add(DeviceType.WEIGHT_SCALE);
			}
		}
		
	}
}
