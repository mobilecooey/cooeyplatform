package com.biz.cooey;

public class CooeyDeviceType {

	public static final int BP_METER = 1;
	public static final int WEIGHT_SCALE = 2;
	public static final int FAT_SCALE = 3;
	public static final int WEIGHT_AND_FAT_SCALE  =4;
}
