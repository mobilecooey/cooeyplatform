package com.biz.cooey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lifesense.ble.bean.SexType;
import com.lifesense.ble.bean.UnitType;

public class CooeyProfile {

	@Expose private String firstName = "";
	@Expose private String lastName = "";
	@Expose private String mobileNumber = "";
	@Expose private String gender= "";
	@Expose @SerializedName("area") private String locationArea = "";
	@Expose @SerializedName("country") private String locationCountry = "";
	@Expose @SerializedName("city") private String locationCity= "";
	@Expose private String email = "";
	@Expose @SerializedName("DOB") private String dob = "1970-01-01";
	@Expose private String bloodGroup = "";
	@Expose @SerializedName("TID") private String tId = "androidapp";
	@Expose private String trackingId = "";
	private long dbid;
	private String nickName = "";
	private int isloggedin = 0;
	@Expose private float weight = 0;
	@Expose private float height = 0;
	private int patientId = 0;
	@Expose private String externalID;
	private long parentId = -1;
	@Expose private int type = 1;
	
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public long getParentId() {
		return parentId;
	}
	public void setParentId(long parentId2) {
		this.parentId = parentId2;
	}
	private int athleteLevel = 0;
	private int isAthlete = 0;
	private float goalWeight = 0;
	private float waistline = 0;
	
	
	public int getAthleteLevel() {
		return athleteLevel;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public long getDbid() {
		return dbid;
	}
	public String getDob() {
		return dob;
	}
	public String getEmail() {
		return email;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getGender() {
		return gender;
	}
	public float getGoalWeight() {
		return goalWeight;
	}
	public float getHeight() {
		return height;
	}
	public int getIsAthlete() {
		return isAthlete;
	}
	
	public int getIsloggedin() {
		return isloggedin;
	}
	public String getLastName() {
		return lastName;
	}
	public String getLocationArea() {
		return locationArea;
	}
	public String getLocationCity() {
		return locationCity;
	}
	
	
	public String getLocationCountry() {
		return locationCountry;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	
	public String getNickName() {
		return nickName;
	}
	public int getPatientId() {
		return patientId;
	}
	public String gettId() {
		return tId;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public float getWaistline() {
		return waistline;
	}
	public float getWeight() {
		return weight;
	}
	public void setAthleteLevel(int athleteLevel) {
		this.athleteLevel = athleteLevel;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public void setGoalWeight(float goalWeight) {
		this.goalWeight = goalWeight;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public void setIsAthlete(int isAthlete) {
		this.isAthlete = isAthlete;
	}
	public void setIsloggedin(int isloggedin) {
		this.isloggedin = isloggedin;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setLocationArea(String locationArea) {
		this.locationArea = locationArea;
	}
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}
	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}
	public void settId(String tId) {
		this.tId = tId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public void setWaistline(float waistline) {
		this.waistline = waistline;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getExternalID() {
		return externalID;
	}
	public void setExternalID(String externalId) {
		this.externalID = externalId;
	}
		
}
