package com.biz.cooey;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;

public class GetJsonFromCooeyTask extends AsyncTask<String, Void, String>{

	private HttpResponse response;
	private SendToCooeyCallbackOnResponse callback;
	HttpClient httpClient = null;
	
	public GetJsonFromCooeyTask(SendToCooeyCallbackOnResponse _cback) {
		callback = _cback;
	}
	
	@Override
	protected String doInBackground(String... params) {
		if(httpClient == null) httpClient = new DefaultHttpClient();
		String responseBody = null;
		HttpGet request = new HttpGet(callback.getUrl());
		try {
			response = httpClient.execute(request);
			
			int responseCode = response.getStatusLine().getStatusCode();
	        switch(responseCode) {
		        case 200:
					
					HttpEntity entity = response.getEntity();
					if(entity != null) {
						responseBody = EntityUtils.toString(entity);
						
						callback.callme(responseBody, responseCode);
					}
		            break;
		        default:
		        	callback.callme(null, responseCode);
		        	break;
		        	
	        }
		} catch (IOException | NetworkOnMainThreadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseBody;
	}

}
