package com.biz.cooey;

import android.content.Context;

public interface SendToCooeyCallbackOnResponse{
	public void callme(String resp, int code);
	public void callmeWithContext(String resp, Context context);
	public String getUrl();
}
