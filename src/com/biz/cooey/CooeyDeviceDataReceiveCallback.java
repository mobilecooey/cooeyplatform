package com.biz.cooey;


public abstract class CooeyDeviceDataReceiveCallback {

	public abstract void onReceiveBPData(BPData bpd);
	public abstract void onReceiveWeightdata(WeightData wd);
}
