package com.biz.cooey;

import com.lifesense.ble.PairCallback;
import com.lifesense.ble.ReceiveDataCallback;
import com.lifesense.ble.bean.LsDeviceInfo;

import android.content.Context;

public abstract class BleDeviceManager {

	public abstract CooeyStatus initialize(CooeyProfile profile, Context arg1);

	public abstract CooeyStatus checkPlatformCapability();

	public abstract void searchDevicesForPairing( CooeyDeviceSearchCallback sc);

	public abstract void pairDevice( LsDeviceInfo d , CooeyDevicePairCallback pc);

	public abstract CooeyStatus registerPairedDevice(LsDeviceInfo arg0);

	public abstract CooeyStatus receiveData( CooeyDeviceDataReceiveCallback drc);

	public abstract CooeyStatus shutdown();
	
	public abstract int getMaxUserProfilesForDevice(LsDeviceInfo arg0);

	public abstract CooeyStatus deRegisterAllDevices();
	
	public abstract CooeyStatus deRegisterPairedDevices(LsDeviceInfo arg0);
	
	public abstract void stopSearchForDevices();
}
