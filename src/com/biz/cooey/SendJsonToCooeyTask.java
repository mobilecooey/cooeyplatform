package com.biz.cooey;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

public class SendJsonToCooeyTask extends AsyncTask<String, Void, Void>{

	private HttpResponse response;
	private SendToCooeyCallbackOnResponse callback;
	HttpClient httpClient = null;
	private Context context = null;
	private ProgressDialog dialog = null;
	private Boolean isForeground = false;
	private String foregroundMessage = "";
	
	public SendJsonToCooeyTask(SendToCooeyCallbackOnResponse _cback) {
		callback = _cback;
	}
	
	public SendJsonToCooeyTask(SendToCooeyCallbackOnResponse _cback, Context ctx, Boolean flag, String msg) {
		callback = _cback;
		this.context = ctx;
		isForeground  = true;
		foregroundMessage = msg;
		dialog = new ProgressDialog(ctx);
	}
	
	protected void onPreExecute() {
		if(isForeground){
			this.dialog.setMessage(foregroundMessage);
			this.dialog.show();
			this.dialog.setCancelable(false);
		}
	}
	
	@Override
	protected Void doInBackground(String... arg0) {
		if(httpClient == null) httpClient = new DefaultHttpClient();
		try {
	        HttpPost request = new HttpPost(callback.getUrl());
	        StringEntity params = new StringEntity(arg0[0]);
	        request.addHeader("Content-Type", "application/json");
	        request.setEntity(params);
	        response = httpClient.execute(request);
	        
	        //
	        if(isForeground){
		        if (dialog.isShowing()) {
					dialog.dismiss();
				}
	        }	        
	        int responseCode = response.getStatusLine().getStatusCode();
	        String responseBody = "{}";
	        switch(responseCode) {
		        case 200:					 
					HttpEntity entity = response.getEntity();
					if(entity != null) {
						responseBody = EntityUtils.toString(entity);
					}
		            //callback.callme(responseBody);
		            break;
		        case 500:
		        	//callback.callme(responseBody);
		        	break;
		        default:
		        	//callback.callme(responseBody);
		        	break;
	        }
	        if(context == null){
	        	callback.callme(responseBody, responseCode);
	        	return null;
	        }
	        callback.callmeWithContext(responseBody, context);
	    }catch (Exception ex) {
	    	httpClient.getConnectionManager().shutdown();
	    } 
		return null;
	}
}